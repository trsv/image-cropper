#include <QCoreApplication>
#include <QtCore>
#include <QImage>

// количество изображений на странице
static const auto IMAGE_ON_PAGE = 12;

// длина и ширина изображения
static const auto WIDTH = 1130;
static const auto HEIGHT = 527;

// создаем список координат каждого изображения
static inline auto create_coords() {
    QList <std::tuple<int, int>> coords;

    for (int i = 0, y = 130; i < IMAGE_ON_PAGE / 2; i++, y += HEIGHT) {
        coords << std::make_tuple(90, y);
        coords << std::make_tuple(1260, y);
    }
    return coords;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug() << "[INFO] Begin work";

    // исходное изображение (страница)
    QImage image("katalog_pas.jpg");

    // список координат
    static const auto coords = create_coords();

    // по списку координат вырезаем изображения
    for (int i = 0; i < coords.size(); i++) {
        // координаты изображения
        const auto & [x, y] = coords[i];
        // вырезаем изображение по координатам и заданному размеру
        image.copy(x, y, WIDTH, HEIGHT).save(QString("out/cropped_%1.jpg").arg(i + 1));
    }

    qDebug() << "[INFO] Finish work";
    return a.exec();
}
